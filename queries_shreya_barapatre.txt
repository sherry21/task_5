1. list of consultants having more than one submission

select c.name from public.consultant c join public.submission s on c.cid = s.cid group by c.cid having count(c.name)>1; 

2. list all the interviews of consultants
select c.cid,c.name,iid,start,"end",result,round,remark,status from ((submission s inner join consultant c on s.cid=c.cid ) join interview i on i.subid=s.subid);

3. list all PO of marketers

select public."Employee".eid,public."Employee".name , purchase_order.* from public."Employee",emp_role,role,purchase_order,submission
where public."Employee".eid=emp_role.eid and role.name='Marketer' and emp_role.rid=role.rid and public."Employee".eid=submission.eid and purchase_order.poid=submission.poid;

4. unique vendor company name for which client location is the same

select distinct(c.cname) as Client_name, v.vname as Vendor_name, s.city as City from client c, vendor v, submission s
where c.cid=s.cliid and v.vid=s.vid order by s.city; 

5. count of consultants which submitted in the same city

select distinct s.city, count(*) over(partition by s.city) as same_city from public.consultant c join public.submission s on c.cid=s.cid;

6. name of consultant and client who have been submitted on the same vendor
select c.name as consultant_name,v.vname as vendor_name, cli.cname as client_name from consultant c,submission sub, client cli, vendor v 
where c.cid=sub.cid and sub.cliid=cli.cid and sub.vid=v.vid order by v.vname;